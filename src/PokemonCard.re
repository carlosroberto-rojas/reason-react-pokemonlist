type  pokemon  =
    {
        weight: int,
        name: string,
        sprite: string
    }

let pokemonDecode = pk =>
     Json.Decode.{
        weight: pk |> field("weight", int),
        name: pk |> field("name", string),
        sprite: pk |> at(["sprites","front_default"], string)
    }

let componente = ReasonReact.statelessComponent("PokemonCard");

let make = (~poke: pokemon, _children) => {
    ...componente,
    render: _self =>
        <div style={
            ReactDOMRe.Style.make(~border="solid black 1px",
                ~borderRadius="15px", ~width="30%",
                ~margin="auto", ~padding="10px",
                ~boxShadow="0 15px 20px rgba(0, 0, 0, 0.3)",
                ~marginBottom = "20px",
                ())
        }>
            <h3 style={
                ReactDOMRe.Style.make(~backgroundColor="black",
                    ~color="white", ~textAlign="center",
                    ~padding="5px 0", ())
            } >
                {ReasonReact.string(poke.name)}
            </h3>
            <p> { ReasonReact.string("Peso:" ++ (poke.weight -> string_of_int))} </p>
            <img src={poke.sprite}/>
        </div>
};