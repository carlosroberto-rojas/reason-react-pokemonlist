type state = {
    listado: array(PokemonCard.pokemon),
    numero: int,
};

type action = 
  | Pedir (int)
  | Cargado(array(PokemonCard.pokemon));

let rec range = (inicio:int, fin:int) =>
    if (inicio > fin) {
        [];
    } else {
        [inicio, ...range(inicio+1, fin)];
    }

let cargar10 = numero => 
    Js.Promise.(
        range(numero, numero+11) |> Array.of_list
            |> Array.map( cont => {
                Fetch.fetch("https://pokeapi.co/api/v2/pokemon/" ++ string_of_int(cont) ++ "/")
                |> then_(Fetch.Response.json)
                |> then_(json => PokemonCard.pokemonDecode(json) |>resolve)})
            |>  all |> then_(arregloPokemon => arregloPokemon |> resolve))

let component = ReasonReact.reducerComponent("PokemonList");

let make = (_children) => {
    ...component,
    initialState : () =>{listado:[||], numero:1},
    
    reducer: (action, state) => 
        switch action {
        | Pedir (numero) => ReasonReact.SideEffects(self =>
        Js.Promise.(cargar10 (numero)
            |> then_(listaPokemon => self.send(Cargado(listaPokemon)) |>resolve) |>ignore))
        | Cargado(lista) => ReasonReact.Update({listado:lista,numero:state.numero+12})
        },
    
        render: self => <div>
            <div style={ReactDOMRe.Style.make(~display="flex",
              ~flexWrap="wrap",  ())}>
                 {(self.state.listado
                    |> Array.map( elem => <PokemonCard poke={elem} />))
                    |> ReasonReact.array}
            </div>
            <button onClick = {_evt => self.send(Pedir(self.state.numero))}>
                    {ReasonReact.string({j|Más Pokemon|j})}
            </button>
            </div>,

    didMount: self => self.send(Pedir(1))
}
